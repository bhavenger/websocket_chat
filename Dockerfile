FROM erlang:20.3.2 as erl-builder

COPY app /usr/src/app
WORKDIR /usr/src/app
RUN rebar3 as prod release

FROM erlang:20.3.2 as erl-runner

LABEL maintainer="bhavenger@gmail.com"

COPY --from=erl-builder /usr/src/app/_build/prod/rel/websocket_chat /opt/websocket_chat
EXPOSE 8080

WORKDIR /opt/websocket_chat
CMD [ "./bin/websocket_chat", "foreground" ]