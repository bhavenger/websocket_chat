include:
    - python-pip

"Ensure that Docker repo is installed":
    pkgrepo.managed:
        - name: deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable
        - file: /etc/apt/sources.list.d/docker-official.list
        - gpgcheck: 1
        - key_url: https://download.docker.com/linux/ubuntu/gpg

"Ensure that Docker package is installed":
    pkg.installed:
        - refresh: True
        - name: docker-ce
        - require:
            - pkgrepo: "Ensure that Docker repo is installed"

"Ensure that Docker service is started":
    service.running:
        - name: docker
        - reload: True
        - init_delay: 5
        - enable: True
        - require:
            - pkg: "Ensure that Docker package is installed"

"Ensure that docker-py is installed":
    pip.installed:
        - name: docker-py
        - upgrade: True
        - require:
            - service: "Ensure that Docker service is started"
            - pkg: "Ensure that python packages are installed"
