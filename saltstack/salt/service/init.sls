include:
    - docker

"Ensure, that image present":
    cmd.run:
        - name: docker build . -t websocket_chat:0.1
        - cwd: /vagrant/
        - require:
            - service: "Ensure that Docker service is started"
            - pkg: "Ensure that python packages are installed"
            - pip: "Ensure that docker-py is installed"

"Ensure, that service is working":
    cmd.run:
        - name: docker run -d -p 8080:8080 websocket_chat:0.1
        - require:
            - cmd: "Ensure, that image present"