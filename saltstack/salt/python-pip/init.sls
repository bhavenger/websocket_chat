"Ensure that python packages are installed":
    pkg.installed:
        - names:
            - python
            - python-dev
            - python-pip